
//Using an object literal for a jQuery feature
var website = {
    config: {
        logoImageClass: 'logo-image',
    },

    // API

    init: function( settings ) {
        website.config = {
            fancy: '[rel="lightbox"]',
            orphans: 'article p',
			animate: '.animated',
            smoothscroll: '.smooth-scroll, .navbar a, .subnav a',
            logoSelector: '.header-logo',
            logoImageClass: website.config.logoImageClass,
            swiperSelector: '.swiper',
            swipers: [],
        };
        //allow overriding the default config
        $.extend( website.config, settings );
        website.setup();
    },
    setup: function( obj ) {
    	if( typeof obj == "undefined" ) obj = $('body');
    	else obj = $(obj);

        website.initHeader();
        website.initSwiper( obj.find( website.config.swiperSelector ) );
        
        website.initFancybox( obj.find( website.config.fancy ) );
        website.initOrphans( obj.find( website.config.orphans ) );
        website.initSmoothScroll( obj.find( website.config.smoothscroll ) );
        website.initAnimate( obj.find( website.config.animate ) );

        // BOOTSTRAP
        $( obj.find('[data-toggle="tooltip"]' ) ).tooltip();
        $( obj.find('[data-toggle="popover"]' ) ).popover();

        obj.find( 'img' ).imagesLoaded( function() {
            setTimeout( function(){ website.reinitAnimate( obj.find( website.config.animate ) ); }, 50 );
        });
    },
    resize: function() {
        website.reinitAnimate( website.config.animate );
    },
    scroll: function() {
        website.initHeader();
    },

    // END API

    //Init navbar functions
	initHeader: function() {
        var scrollTop = $(document).scrollTop();
        var limitTop = 1; //$( 'header.main-header' ).height();

        if ( scrollTop > limitTop ){
            $( 'header.main-header' ).addClass('scroll');
        } else {
            $( 'header.main-header' ).removeClass('scroll');
        }
	},

    //Init Swiper
    initSwiper: function (obj) {
        if(!Swiper) return;
        var swipersSync = [], swipersControl = {};
        $(obj).each(function (id, el) {
            var $el = $(el);
            var data = $el.data();
            if(data.pagination)
            {
                for(var ind1= 0, funName1, keys1 = Object.keys(data.pagination); funName1 = data.pagination[keys1[ind1]]; ind1++ )
                {
                    if(keys1[ind1].indexOf('render') > -1)
                    {
                        data.pagination[keys1[ind1]] = eval('window["'+funName1.replace('.','"]["')+'"]') || funName1;
                    }
                }
            }
            var pushSwiper = data.autoplay && data.swiperSyncId;
            if(pushSwiper)
            {
                if(!swipersSync[data.swiperSyncId])
                {
                    swipersSync[data.swiperSyncId] = {
                        autoplay: data.autoplay,
                        sliders: []
                    };
                }
                delete data.autoplay;
            }
            var elId = $el.attr('id') ? $el.attr('id') : 'bx'+ Object.keys(website.config.swipers).length;
            if(data.on){
                for(var ind = 0, funName, keys = Object.keys(data.on); funName = data.on[keys[ind]]; ind++ )
                {
                    data.on[keys[ind]] = eval('window["'+funName.replace('.','"]["')+'"]') || funName;
                }
                if(data.on.init)
                {
                    data.init = false;
                }
            }
            if(data.customControl)
            {
                swipersControl[elId] = data.customControl;
            }
            website.config.swipers[elId] = new Swiper(this,$.extend({
                loop: true
            },data));
            if(data.on && data.on.init)
            {
                website.config.swipers[elId].init();
            }
            if(pushSwiper) swipersSync[data.swiperSyncId].sliders.push(this.swiper);
        });

        for(var fu = 0, el, keys = Object.keys(swipersSync); el = swipersSync[keys[fu]]; fu++ ){
            setInterval(function () {
                for(var i = 0, item; item = this[i]; i++) item.slideNext();
            }.bind(el.sliders), (el.sliders[0].speed || 300) + (el.autoplay.delay || 5000));
        }
        var keys = Object.keys(swipersControl);
        if(keys.length)
        {
            for(var i = 0, key; key = keys[i]; i++ )
            {
                var id = swipersControl[key];
                if(website.config.swipers[key] && website.config.swipers[id] )
                {
                    try {
                        website.config.swipers[key].controller.control = website.config.swipers[id];
                    }
                    catch (ex) {
                        try {
                            website.config.swipers[key].params.control = website.config.swipers[id];
                        } catch (e) {}
                    }
                }
            }
        }
    },

    //Init all fancy box galleries based on rel=lightbox-gallery
    initFancybox: function( obj ) {

        if( !$.isFunction( $.fn.fancybox ) ) return;

        	$(obj).each(function( id, el ) {
        	    var id = 'fancy-'+Math.floor((Math.random() * 1000) + 1);
        	    var selector = '#'+id;
                $(el).attr('id', id);

                setTimeout(function(){
                    $( selector ).fancybox( $.extend( {
                        'transitionIn'	: 'elastic',
                        'transitionOut'	: 'elastic',
                        'titlePosition' : 'inside',
                        'overlayColor' : '#fff',
                        'overlayOpacity' : '0.9',
                        'smallBtn' : true,
                        'toolbar' : false,
                        'iframe' : {
                            scrolling : 'auto',
                            preload   : false
                        }
                    }, $(el).data() ) );
                }, 100);
    		});
        	
        website.initFancyboxGallery();
    },
    
    //Init all fancy box galleries based on rel=lightbox-gallery
    initFancyboxGallery: function() {
        if( !$.isFunction( $.fn.fancybox ) ) return;
        
        	var obj = '.fancybox';
        	var galleries = {};
        	$(obj).each(function( id, el ) {
        		var rel = $(el).attr('rel');
        		eval( 'galleries.'+rel+' = "'+rel+'"' );
    		});
        	
        	$.each( galleries, function( id, el ) {

        		$( '[rel='+el+']' ).fancybox( $.extend( {
                    'transitionIn'	: 'elastic',
                    'transitionOut'	: 'elastic',
                    'titlePosition' : 'inside',
                    'overlayColor' : '#fff',
                    'overlayOpacity' : '0.9',
                    'smallBtn' : true,
                    'toolbar' : false,
        		}, $(el).data() ) );
        		
    		});
    },

    //Replace spaces before short words to unbreakable spaces
    initOrphans: function( obj ){
        $( obj ).each(function( id, el ) {
            content = $(el).html();
            if( content && content.length > 0) {
                content = content.replace(/(\s|&nbsp;)([^\s<>"'`]{1,2})(\s)/g, ' $2$3');
                content = content.replace(/((\s)–|–(\s))/g, '$2-$3');

                replaced = content.replace(/(\s)([^\s<>"'`]{1,2})\s+/g, "$1$2&nbsp;");
                $(el).html(replaced);
            }
        });
    },

    //Init smooth scroll plugin
    initSmoothScroll: function( obj ) {
        if( !$.isFunction( $.fn.smoothScroll ) ) return;

		$(obj).smoothScroll( { easing: 'easeInOutExpo', speed: 1000 } );
    },

    //Re-init animation library
    reinitAnimate: function( obj ) {
        //remove class with animation-duration in order to reset position of element
        $(obj).removeClass('animate-init');
        //important when rotating the tablet. Waypoint must be reinitialized
        //remove class with animation-name in order to reset position of element
        $(obj).filter('.animated[data-animate-event-class2]').each(function () {
            var item = $(this);
            var data = item.data();
            item.removeClass(data.animateEventClass + ' ' + data.animateEventClass2);
        });
        website.initAnimate( obj );
    },

    //Init animation library
    initAnimate: function( obj ) {
        $(obj).each(function( id, el ) {

            if($(el).hasClass('animate-init')) return;

            // OS X Chrome fix
            if(
                navigator.platform.toUpperCase().indexOf('MAC') >= 0
                &&
                navigator.vendor.toUpperCase().indexOf('APPLE') >= 0
                &&
                $(el).hasClass('disable-animation-on-apple')
            )
            {
                $(el).removeClass('animated opacity-0').css('opacity','1');
                return;
            }

            $(el).addClass('animate-init');

            var type = $(el).data('animate-class');
            if( typeof type === "undefined" ) type = 'fadeIn';
            var delay = $(el).data('animate-delay');
            var offset = $(el).data('animate-offset');

            var event = $(el).data('animate-event');
            var eventType = $(el).data('animate-event-class') || 'fadeInUpMedium';
            var event2 = $(el).data('animate-event2');
            var eventType2 = $(el).data('animate-event-class2') || 'fadeOutDown';

            if(delay == 'waypoint') {
                if(!event)
                    event = 'waypointIn';

                if(!event2)
                    event2 = 'waypointOut';

                $(el).css('opacity', 0);
                $(el).removeClass(eventType+' '+eventType2+' '+type);

                var waypoints = $(el).waypoint({
                    handler: function(direction) {

                        if(direction == 'down') {
                            $(el).trigger(event);
                        } else {
                            $(el).trigger(event2);
                        }
                    },
                    offset: ( offset ? offset+'%' : '85%' )
                });

            } else if(delay) {
                $(el).css('opacity', 0);
                setTimeout( function(){
                    $(el).css('opacity', 1);
                    $(el).removeClass( type ).addClass( type);
                }, delay );
            } else {
                $(el).removeClass( type ).addClass( type);
            }

            if( event ) {
                $(el).bind( event, function() {
                    $(el).removeClass( type ).removeClass( eventType ).removeClass( eventType2 ).addClass( eventType );
                });
            }

            if( event2 ) {
                $(el).bind( event2, function() {
                    $(el).removeClass( type ).removeClass( eventType ).removeClass( eventType2 ).addClass( eventType2 );
                });
            }

            // OS X Chrome fix
            if(
                navigator.platform.toUpperCase().indexOf('MAC') >= 0
                &&
                navigator.vendor.toUpperCase().indexOf('GOOGLE') >= 0
            )
            {
                $(el).on('webkitAnimationEnd', function(){
                    setTimeout(function(){
                        this.item.removeClass(this.name).addClass(this.fixes).css('opacity','1')[0].style.webkitAnimationName='';
                    }.bind(this),100);
                }.bind({
                    item: $(el),
                    name: 'animated opacity-0 fadeIn fadeInUp fadeInUpMedium',
                    fixes: 'animated-fixes'
                }));
            }

        });
    }
};

(function(){
    // PARALAX SCROLL

    $(function() {
        if (!window.lax) {
            return;
        }

        window.lax.setup() // init
    
        var updateLax = () => {
            window.lax.update(window.scrollY)
            window.requestAnimationFrame(updateLax)
        }
    
        window.requestAnimationFrame(updateLax)
    });
(function(){
})();
    // SMOOTH SCROLL 

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        return;
    }
    
    $(function(){
        var body = document.body;
        var main = document.querySelector("body > main");
        var sx = 0, // For scroll positions
            sy = 0;
        var dx = sx, // For container positions
            dy = sy;

        body.style.height = main.clientHeight + "px";

        main.style.position = "fixed";
        main.style.top = 0;
        main.style.left = 0;

        // Bind a scroll function
        window.addEventListener("scroll", easeScroll);

        function easeScroll() {
            sx = window.pageXOffset;
            sy = window.pageYOffset;
        }

        window.requestAnimationFrame(render);

        function render(){
            //We calculate our container position by linear interpolation method
            dx = li(dx,sx,0.15);
            dy = li(dy,sy,0.15);
            
            dx = Math.floor(dx * 100) / 100;
            dy = Math.floor(dy * 100) / 100;
            
            main.style.transform = `translate3d(-${dx}px, -${dy}px, 0px)`;
            
            window.requestAnimationFrame(render);
        }

        function li(a, b, n) {
            return (1 - n) * a + n * b;
        }
    });
    
    function resize() {
        var body = document.body;
        var main = document.querySelector("body > main");
        body.style.height = main.clientHeight + "px";
    }
    $(window).on('resize', function(){
        resize();
        setTimeout(resize, 500);
    });
    setTimeout(resize, 500);
    setTimeout(resize, 1000);
})();

$( document ).ready( website.init );
$( document ).load( website.setup );
$( window ).resize( website.resize );
$( window ).scroll( website.scroll );
